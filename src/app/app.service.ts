import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { SseService } from './sse.service';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  constructor(private zone: NgZone, private sseService: SseService) {}

  getEventsFromServer(url: string) {
    return new Observable((observer) => {
      const eventSource = this.sseService.getEventService(url);
      eventSource.onmessage = (e) => {
        console.log('eee');
        this.zone.run(() => {
          observer.next(e);
        });
      };
      eventSource.onerror = (err) => {
        this.zone.run(() => {
          observer.error(err);
        });
      };
    });
  }
}
