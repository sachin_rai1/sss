import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'sse';
  constructor(private service: AppService) {
    const evtSource = new EventSource('http://localhost:3000/stream');

    evtSource.onopen = (e) => {
      console.log('connection open');
      console.log(e);
    };
    evtSource.onmessage = (e) => {
      console.log('connection message');
      console.log(e.data);
    };
    evtSource.onerror = (e) => {
      console.log('connection error');
      console.log(e);
      evtSource.close();
    };
  }

  ngOnInit() {
    this.service
      .getEventsFromServer('http://localhost:3000/stream')
      .subscribe((data) => console.log('data from server', data));
  }
}
