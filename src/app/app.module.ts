import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { SseService } from './sse.service';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [AppService, SseService],
  bootstrap: [AppComponent],
})
export class AppModule {}
