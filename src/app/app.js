const express = require("express");
const parser = require("body-parser");
const app = express();

const EventEmitter = require("events");

const Stream = new EventEmitter();
var cors = require("cors");
app.use(cors());
app.use(parser.json());
app.use(parser.urlencoded({ extended: true }));

app.get("/stream", function (request, response) {
  response.writeHead(200, {
    "Content-Type": "text/event-stream",

    Connection: "keep-alive",
  });

  Stream.on("push", function (event, data) {
    //don't forget to flush
    response.write(
      "event: " +
        String(event) +
        "\n" +
        "data: " +
        JSON.stringify(data) +
        "\n\n"
    );

    // out.flush();
  });
});

setInterval(() => {
  Stream.emit("push", "message", { msg: "price  Chnage" });
}, 10000);

app.listen(3000, () => {
  console.log("running at Complete");
});
