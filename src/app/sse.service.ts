import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})

  //The service above will just simply create an
  //EventSource that will receive events from the server.
export class SseService {
  constructor() {}

  getEventService(url: string): EventSource {
    return new EventSource(url);
  }

}
